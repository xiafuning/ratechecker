#ifndef __IEEE80211_H__
#define __IEEE80211_H__

#include "ieee80211_radiotap.h"
#include "cfg80211.h"
#include "linux_ieee80211.h"

struct ieee80211_llc_hdr {
	u8 dsap;
	u8 ssap;
	u8 control_field;
	u8 organization_code[3];
	__le16 ethernet_type;
} __packed;

int build_ieee80211(struct sk_buff *skb, struct packet_info *pinfo);
int parse_ieee80211(struct sk_buff *skb, struct packet_info *pinfo);


#endif
