#ifndef __RADIOTAP_H__
#define __RADIOTAP_H__

int parse_radiotap(struct sk_buff *skb, struct packet_info *pinfo);
int build_radiotap(struct sk_buff *skb, struct packet_info *pinfo);

#endif
