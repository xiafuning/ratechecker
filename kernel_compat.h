#ifndef __KERNEL_COMPAT_H__
#define __KERNEL_COMPAT_H__

#include <byteswap.h>

#define s8 int8_t
#define s16 int16_t
#define s32 int32_t
#define s64 int64_t
#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define u64 uint64_t

#define __aligned(x)
#define unlikely(x) x
#define bool int
#define true 1
#define false 0

#define __unused	__attribute__ ((unused))

/* various hacks */
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define get_unaligned_le16(x) (*((u16 *) x))
#define get_unaligned_le32(x) (*((u32 *) x))
#define cpu_to_le16(x)	(x)
#define cpu_to_le16(x)	(x)
#define cpu_to_le32(x)	(x)
#define cpu_to_le32(x)	(x)
#else
#define get_unaligned_le16(x) (bswap_16(*((u16 *) x)))
#define get_unaligned_le32(x) (bswap_32(*((u32 *) x)))
#define cpu_to_le16(x)	(bswap_16(x))
#define le16_to_cpu(x)	(bswap_16(x))
#define cpu_to_le32(x)	(bswap_32(x))
#define le32_to_cpu(x)	(bswap_32(x))
#endif


#define EXPORT_SYMBOL(x) 

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#define __packed __attribute((packed))   /* linux kernel compat */


#endif /* __KERNEL_COMPAT_H__ */
